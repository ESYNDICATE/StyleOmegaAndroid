package com.company.ezdelivery.app_shopping.controller;

import com.company.ezdelivery.app_shopping.model.Cart_model;
import com.company.ezdelivery.app_shopping.model.Items;

import java.util.List;

public class CartModelController {

    private Cart_model cart_model;
    private Items items;
    private String user_email;

    public CartModelController(Cart_model cart_model) {
        this.cart_model = cart_model;
    }

    public Items getItems() {
        return items;
    }

    public void setItems(Items items) {
        this.items = items;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public boolean isCartEmpty(String email){
        boolean flag = false;
        if(Cart_model.find(Cart_model.class,"email = ?",email).size() == 0){
            flag = true;
        }
        return flag;
    }

    public List<Cart_model> itemsInCart(String email){

        List<Cart_model> itemsList = Cart_model.find(Cart_model.class,"email = ?", email);

        return itemsList;
    }

    public void addToCart(){
        Cart_model.save(cart_model);
    }

    public void removeFromCart(){
        Cart_model.delete(cart_model);
    }
}
