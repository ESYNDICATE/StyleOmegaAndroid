package com.company.ezdelivery.app_shopping;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.company.ezdelivery.app_shopping.adapter.CartAdapter;
import com.company.ezdelivery.app_shopping.model.Cart_model;
import com.company.ezdelivery.app_shopping.model.Items;

import java.util.List;

public class Cart extends AppCompatActivity {

    ListView listView;
    List<Cart_model> cart_models;
    String email="";
    Button checkout_button;
    private Button continue_button;
    TextView lbl_price;
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        lbl_price = findViewById(R.id.lbl_price);
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        email = sharedPreferences.getString("email", "");
        cart_models = Cart_model.find(Cart_model.class,"user_email = ?", email);
        listView = findViewById(R.id.cart_list);
        continue_button = (Button) findViewById(R.id.continue_btn);
        final CartAdapter adapter = new CartAdapter(cart_models,Cart.this);


        listView.setAdapter(adapter);
        if(cart_models.size() != 0){
            setTotalPrice();
        }


        checkout_button = findViewById(R.id.checkout_btn);

        checkout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(Cart.this,"Checking Out",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Cart.this, Checkout.class);
                startActivity(intent);
            }
        });

        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Cart.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    public void setTotalPrice(){
        double total_price = 0;
        for(int i = 0; i < cart_models.size(); i++){
            total_price += cart_models.get(i).getItems().getPrice();
        }
        lbl_price.setText(Double.toString(total_price));
    }
}
