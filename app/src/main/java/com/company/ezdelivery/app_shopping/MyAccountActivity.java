package com.company.ezdelivery.app_shopping;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.company.ezdelivery.app_shopping.controller.UserController;

import java.util.regex.Pattern;

public class MyAccountActivity extends AppCompatActivity {

    //Declaring components
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    EditText fName, lName,dob;
    Button btn_submit , btn_back;
    private View mProgressView;
    private View myAccountFormView;
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawyer_my_account);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Getting the components based on the ID
        fName = findViewById(R.id.user_first_name_txt);
        lName = findViewById(R.id.user_last_name_txt);
        dob = findViewById(R.id.user_date_of_birth_txt);
        btn_submit = findViewById(R.id.submit_button);
        btn_back = findViewById(R.id.back_btn);

        myAccountFormView = findViewById(R.id.my_account_form);
        mProgressView = findViewById(R.id.my_account_progress);

        //Setting the user details
        setUserValues();

        submitAction();

        backBtnAction();
    }

    public void backBtnAction(){

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Setting the redirect page to the Login Activity Page
                Intent intent = new Intent(MyAccountActivity.this, HomeActivity.class);
                //Executing the method of redirecting
                startActivity(intent);
                finish();
            }
        });

    }

    public void setUserValues(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Getting the email
        String email = sharedPreferences.getString("email","");

        UserController userController = new UserController();
        userController.setEmail(email);
        userController.setUserDetailsBasedOnEmail();

        //Setting the values
        fName.setText(userController.getFirst_name());
        lName.setText(userController.getLast_name());
        dob.setText(userController.getDate_birth_date());
    }

    public void submitAction(){
//On click listener for the register button
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Getting the user input details for local variables
                String first_name = fName.getText().toString().trim();
                String last_name = lName.getText().toString().trim();
                String strdob = dob.getText().toString().trim();

                // Reset errors.
                fName.setError(null);
                lName.setError(null);
                dob.setError(null);
                btn_submit.setError(null);

                boolean cancel = false;
                View focusView = null;



                // Check if the first name is empty.
                if (TextUtils.isEmpty(first_name)) {
                    fName.setError(getString(R.string.error_field_required));
                    focusView = fName;
                    cancel = true;
                }

                // Check if the last name is empty.
                if (TextUtils.isEmpty(last_name)) {
                    lName.setError(getString(R.string.error_field_required));
                    focusView = lName;
                    cancel = true;
                }


                // Check if the date oof birth is empty.
                if (TextUtils.isEmpty(strdob)) {
                    dob.setError(getString(R.string.error_field_required));
                    focusView = dob;
                    cancel = true;
                }else if (!isDOBValid(strdob)) {
                    dob.setError(("Inavlid date format"));
                    focusView = dob;
                    cancel = true;
                }


                if (cancel) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                } else {
                    // Show a progress spinner, and kick off a background task to
                    // perform the user login attempt.
                    showProgress(true);

                    //Creating the Shared Preference object
                    SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    //Getting the email
                    String email = sharedPreferences.getString("email","");

                    //Creating an object of the user controller
                    UserController userController = new UserController();
                    //Setting the values to the created user controller
                    userController.setFirst_name(first_name);
                    userController.setLast_name(last_name);
                    userController.setDate_birth_date(strdob);
                    userController.setEmail(email);

                    //Calling the update method in the user controller object
                    userController.updateUserDetails();


                    String fullname = userController.getFullNameBasedOnEmail();

                    //Setting the values to shared preference
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("full_name", fullname);
                    editor.commit();

                    //Display the successful message
                    Toast.makeText(MyAccountActivity.this, "Successfully Updated", Toast.LENGTH_SHORT).show();
                    //Setting the redirect page to the Login Activity Page
                    Intent intent = new Intent(MyAccountActivity.this, MyAccountActivity.class);
                    //Executing the method of redirecting
                    startActivity(intent);
                }
            }
        });
    }

    //Check if the email is valid
    private boolean isDOBValid(String dob) {
        //TODO: Replace this with your own logic
        String dateOfBirthRegex = "([0-9]{2})/([0-9]{2})/([0-9]{4})";

        Pattern pat = Pattern.compile(dateOfBirthRegex);

        return pat.matcher(dob).matches();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            myAccountFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            myAccountFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    myAccountFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            myAccountFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
