package com.company.ezdelivery.app_shopping;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.company.ezdelivery.app_shopping.controller.UserController;
import com.company.ezdelivery.app_shopping.model.Users;

import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    //Declaring components
    EditText fName, lName, e_mail, pass, confirmpass,dob;
    Button btn_register;
    private View mProgressView;
    private View mRegisterFormView;
    private Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Getting the components based on the ID
        fName = findViewById(R.id.first_name_txt);
        lName = findViewById(R.id.last_name_txt);
        e_mail = findViewById(R.id.email_txt);
        pass = findViewById(R.id.password_txt);
        confirmpass = findViewById(R.id.confirm_password_txt);
        dob = findViewById(R.id.date_of_birth_txt);
        btn_register = findViewById(R.id.email_register_in_button);
        backButton = (Button) findViewById(R.id.back_button);

        mRegisterFormView = findViewById(R.id.register_form);
        mProgressView = findViewById(R.id.register_progress);

        //Calling the local function of register
        registeringFunction();
        back_function();

    }

    public void registeringFunction(){
        //On click listener for the register button
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Getting the user input details for local variables
                String first_name = fName.getText().toString().trim();
                String last_name = lName.getText().toString().trim();
                String eMail = e_mail.getText().toString().trim();
                String strpass = pass.getText().toString().trim();
                String strconfirmPass = confirmpass.getText().toString().trim();
                String strdob = dob.getText().toString().trim();

                // Reset errors.
                fName.setError(null);
                lName.setError(null);
                e_mail.setError(null);
                pass.setError(null);
                confirmpass.setError(null);
                dob.setError(null);
                btn_register.setError(null);

                boolean cancel = false;
                View focusView = null;


                // Check for a valid email address.
                if (TextUtils.isEmpty(eMail)) {
                    e_mail.setError(getString(R.string.error_field_required));
                    focusView = e_mail;
                    cancel = true;
                } else if (!isEmailValid(eMail)) {
                    e_mail.setError(getString(R.string.error_invalid_email));
                    focusView = e_mail;
                    cancel = true;
                }
                /*
                else if(doesEmailExist(eMail)){
                    e_mail.setError(getString(R.string.error_existing_email));
                    focusView = e_mail;
                    cancel = true;
                }
*/

                // Check if the password is empty.
                if (TextUtils.isEmpty(strpass)) {
                    pass.setError(getString(R.string.error_field_required));
                    focusView = pass;
                    cancel = true;
                }

                // Check if the first name is empty.
                if (TextUtils.isEmpty(first_name)) {
                    fName.setError(getString(R.string.error_field_required));
                    focusView = fName;
                    cancel = true;
                }

                // Check if the last name is empty.
                if (TextUtils.isEmpty(last_name)) {
                    lName.setError(getString(R.string.error_field_required));
                    focusView = lName;
                    cancel = true;
                }

                // Check if the confirm password is empty.
                if (TextUtils.isEmpty(strconfirmPass)) {
                    confirmpass.setError(getString(R.string.error_field_required));
                    focusView = confirmpass;
                    cancel = true;
                }

                // Check if the date oof birth is empty.
                if (TextUtils.isEmpty(strdob)) {
                    dob.setError(getString(R.string.error_field_required));
                    focusView = dob;
                    cancel = true;
                }else if (!isDOBValid(strdob)) {
                    dob.setError(("Inavlid date format"));
                    focusView = dob;
                    cancel = true;
                }


                if (cancel) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                } else {
                    // Show a progress spinner, and kick off a background task to
                    // perform the user login attempt.
                    showProgress(true);

                    //Creating an object of the user controller
                    UserController userController = new UserController();
                    //Setting the values to the created user controller
                    userController.setFirst_name(first_name);
                    userController.setLast_name(last_name);
                    userController.setEmail(eMail);
                    userController.setPassword(strpass);
                    userController.setConfirm_password(strconfirmPass);
                    userController.setDate_birth_date(strdob);

                    //Check if the password and the confirm password equals
                    if (userController.validConfirmPassword()) {
                        //Calling the register method in the user controller object
                        userController.register_user();
                        //Display the successful message
                        Toast.makeText(RegisterActivity.this, "Successfully Registered", Toast.LENGTH_SHORT).show();
                        //Setting the redirect page to the Login Activity Page
                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        //Executing the method of redirecting
                        startActivity(intent);
                        finish();
                    } else {
                        //Displaying the error message
                        Toast.makeText(RegisterActivity.this, "Passwords do not Match!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    //Check if the email is valid
    private boolean isDOBValid(String dob) {
        //TODO: Replace this with your own logic
        String dateOfBirthRegex = "([0-9]{2})/([0-9]{2})/([0-9]{4})";

        Pattern pat = Pattern.compile(dateOfBirthRegex);

        return pat.matcher(dob).matches();
    }

    //Check if the dob is valid
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);

        return pat.matcher(email).matches();
    }

    private boolean doesEmailExist(String email){
        UserController userController = new UserController();
        //Setting the email
        userController.setEmail(email);
        boolean flag =  userController.email_exists(); // Checking whether email already exists in database
        return flag;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRegisterFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void back_function(){
        //On click listener for the back button
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Setting the intent to redirect the page
                Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
                //Redirecting the page
                startActivity(intent);



            }
        });
    }

}
