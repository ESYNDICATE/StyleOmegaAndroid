package com.company.ezdelivery.app_shopping.model;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

@Table(name = "items_title")
public class Items extends SugarRecord{

    @Column(name = "item_id")
    private int item_id;
    @Column(name = "item_name")
    private String item_name;
    @Column(name = "picture_url")
    private String picture_url;
    @Column(name = "colour")
    private String colour;
    @Column(name = "item_category")
    private  String item_category;
    @Column(name = "clothing_size")
    private String clothing_size;
    @Column(name = "price")
    private double price;
    @Column(name = "item_quantity")
    private int item_quantity;

    public Items() {
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getItem_category() {
        return item_category;
    }

    public void setItem_category(String item_category) {
        this.item_category = item_category;
    }

    public String getClothing_size() {
        return clothing_size;
    }

    public void setClothing_size(String clothing_size) {
        this.clothing_size = clothing_size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getItem_quantity() {
        return item_quantity;
    }

    public void setItem_quantity(int item_quantity) {
        this.item_quantity = item_quantity;
    }
}
