package com.company.ezdelivery.app_shopping.controller;

import com.company.ezdelivery.app_shopping.model.Users;

import java.util.List;

public class UserController {
    //Variables
    private Users usermodel;
    private int id;
    private String first_name;
    private String last_name;
    private String password;
    private String confirm_password;
    private String date_birth_date;
    private String email;

    //Constructor
    public UserController(){
        this.usermodel = new Users();
    }

    //Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setUsermodel(Users usermodel) {
        this.usermodel = usermodel;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }

    public void setDate_birth_date(String date_birth_date) {
        this.date_birth_date = date_birth_date;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    //Getters

    public int getId() {
        return id;
    }

    public Users getUsermodel() {
        return usermodel;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public String getDate_birth_date() {
        return date_birth_date;
    }

    public String getEmail() {
        return email;
    }

    //Methods

    public boolean validConfirmPassword(){
        //Creating a local flag variable
        boolean status = false;

        //Checking if the user entered passwords equals
        if(this.password.equals(this.confirm_password)){
            //Setting the local variable to true
            status = true;
        }

        //Returning the local variable
        return status;
    }

    //Function of registering the user
    public void register_user(){
        //Setting the values to the creates user model object
        this.usermodel.setFirst_name(this.getFirst_name());
        this.usermodel.setLast_name(this.getLast_name());
        this.usermodel.setEmail(this.getEmail());
        this.usermodel.setDob(this.getDate_birth_date());
        this.usermodel.setPassword(this.getPassword());
        //calling the save method in user model
        this.usermodel.save();
    }

    //Function to check whether the email exists in the database
    public boolean email_exists(){
        //Creating a local flag variable
        boolean status = false;

        //Getting the result value of the query of based on email and the password
        List<Users> usersList = this.getAllUsersBasedOnEmail();

        //Check if the result value is empty where size will equals to zero
        if(usersList.size() == 0){
            //Setting the local variable to true
            status = false;
        }else{
            //Setting the local variable to false
            status = true;
        }

        //Returning the local variable
        return status;
    }

    //Function to check whether the login is successful
    public boolean login_is_successful(){
        //Creating a local flag variable
        boolean status = false;

        //Getting the result value of the query of based on email and the password
        List<Users> usersList = this.getAllUsersBasedOnEmailAndPassword();

        //Check if the result value is empty where size will equals to zero
        if(usersList.size() == 0){
            //Setting the local variable to true
            status = false;
        }else{
            //Setting the local variable to false
            status = true;
        }

        //Returning the local variable
        return status;
    }


    //Function to get the full name based on the email
    public String getFullNameBasedOnEmail(){
        //Crating a local String variable
        String full_name="";
        //Getting the result as a list
        List<Users> users = getAllUsersBasedOnEmail();

        full_name = users.get(0).getFirst_name() + " " + users.get(0).getLast_name();

        //Returning the created local variable
        return full_name;
    }

    //Set the id based on the email
    public void setIDBasedOnEmail(){
        //Getting the result as a list
        List<Users> users = getAllUsersBasedOnEmail();

        //Getting the id
        int id = Integer.parseInt(users.get(0).getId().toString());

        //Setting the id
        this.setId(id);
    }

    //Getting the user details based on the email
    public void setUserDetailsBasedOnEmail(){
        //Setting the id
        setIDBasedOnEmail();

        this.usermodel = Users.findById(Users.class,this.getId());

        this.setFirst_name(this.usermodel.getFirst_name());
        this.setLast_name(this.usermodel.getLast_name());
        this.setDate_birth_date(this.usermodel.getDob());
    }

    //Updating the user details
    public void updateUserDetails(){
        //Setting the id
        setIDBasedOnEmail();

        this.usermodel = Users.findById(Users.class,this.getId());

        this.usermodel.setFirst_name(this.getFirst_name());
        this.usermodel.setLast_name(this.getLast_name());
        this.usermodel.setDob(this.getDate_birth_date());

        this.usermodel.save();

    }

    //Updating the password
    public void updateUserPasswordDetails(String new_password){
        //Setting the id
        setIDBasedOnEmail();

        this.usermodel = Users.findById(Users.class,this.getId());

        this.usermodel.setPassword(new_password);

        this.usermodel.save();

    }

    //Setting the current password
    public void setCurrentPasswordBasedOnEmail(){
        //Getting the result as a list
        List<Users> users = getAllUsersBasedOnEmail();

        //Getting the password
        String currentPassword = users.get(0).getPassword().toString();

        //Setting the password
        this.setPassword(currentPassword);
    }

    //Custom SQL queries
    public List<Users> getAllUsersBasedOnEmail(){
        //Creating the custom query with the find method and the where clause
        List<Users> usersList = Users.find(Users.class,"email = ?",this.getEmail());
        //returning the list
        return usersList;
    }
    public List<Users> getAllUsersBasedOnEmailAndPassword(){
        //Creating the custom query with the find method and the where clause
        List<Users> usersList = Users.find(Users.class,"email = ? and password = ?",this.getEmail(),this.getPassword());
        //returning the list
        return usersList;
    }
}
