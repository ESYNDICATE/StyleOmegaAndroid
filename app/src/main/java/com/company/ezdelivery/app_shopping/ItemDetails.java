package com.company.ezdelivery.app_shopping;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.ezdelivery.app_shopping.controller.CartModelController;
import com.company.ezdelivery.app_shopping.controller.ItemController;
import com.company.ezdelivery.app_shopping.model.Cart_model;
import com.company.ezdelivery.app_shopping.model.Items;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ItemDetails extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{

    private TextView item_name;
    private TextView item_colour;
    private TextView item_category;
    private TextView item_size;
    private TextView item_price;
    private ImageView item_picture;
    private ItemController itemController;
    private CartModelController cartModelController;
    private Cart_model cart_model;
    private Button add_to_cart_btn;
    private Button share_btn;
    Items items_for_cart;
    private int item_ID;
    private EditText no_of_items_selected;


    //----------------------------Navigation Drawer items--------------------------------
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private TextView userNameView;
    private TextView userEmailView ;
    private MenuItem nav_logout_menu;
    private MenuItem nav_my_account_menu;
    private MenuItem nav_change_password_menu;
    public static final String MyPREFERENCES = "MyPrefs" ;
    private MenuItem cart_menu;
    //----------------------------End of navigation items----------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        item_name = findViewById(R.id.item_name);
        item_colour = findViewById(R.id.item_colour);
        item_category = findViewById(R.id.item_category);
        item_size = findViewById(R.id.item_size);
        item_price = findViewById(R.id.item_price);
        item_picture = findViewById(R.id.itemPicture);
        itemController = new ItemController();
        cart_model = new Cart_model();
        add_to_cart_btn = findViewById(R.id.btn_AddToCart);
        item_ID = getIntent().getIntExtra("ITEM_ID",item_ID);
        items_for_cart = itemController.getItem(item_ID);
        no_of_items_selected = findViewById(R.id.no_of_items);
        share_btn = (Button)findViewById(R.id.btn_share);

        //----------------------------Navigation components----------------------------
        setNavigationViewListner();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawyer);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        userEmailView = (TextView) headerView.findViewById(R.id.userEmailID);
        userNameView = (TextView) headerView.findViewById(R.id.userNameID);

        Menu menuNav=navigationView.getMenu();
        nav_logout_menu = menuNav.findItem(R.id.logout_menu);
        nav_my_account_menu = menuNav.findItem(R.id.my_account_menu);
        nav_change_password_menu = menuNav.findItem(R.id.change_password_menu);
        cart_menu = menuNav.findItem(R.id.cart_menu);

        setSharedPreferenceValues();

        //----------------------------End of navigation components----------------------------


        setItem();

        add_to_cart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addTocart();
                Intent intent = new Intent(ItemDetails.this, Cart.class);
                startActivity(intent);
                finish();


            }
        });

        //Setting the ad to cart button
        setAddToCartButton();
        setShareButton();

    }

    public void setShareButton(){
        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int item_id = getIntent().getIntExtra("ITEM_ID", item_ID);

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT,"/Item_Details?item_id = "+item_id);
                intent.setType("text/plain");
                startActivity(Intent.createChooser(intent,"Share the link"));
            }
        });
    }

    //----------------------------Navigation methods----------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Function to set the values retreiving from the shared preference
    public void setSharedPreferenceValues(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String fullname="";
        String email="";

        if(sharedPreferences.contains("email")){
            email = sharedPreferences.getString("email","");
            fullname = sharedPreferences.getString("full_name","");
            //Enabling the view for un registered users
            nav_logout_menu.setVisible(true);
            nav_my_account_menu.setVisible(true);
            nav_change_password_menu.setVisible(true);
            cart_menu.setVisible(true);

        }else{
            fullname = "Guest";
            email = " ";
            //Disabling the view for un registered users
            nav_logout_menu.setVisible(false);
            nav_my_account_menu.setVisible(false);
            nav_change_password_menu.setVisible(false);
            cart_menu.setVisible(false);

        }


        //Setting the values
        userEmailView.setText(email);
        userNameView.setText(fullname);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout_menu: {
                //Calling the logout function
                logout_function();
                break;
            }

            case R.id.home_menu: {
                //Calling the home function
                home_function();
                break;
            }

            case R.id.my_account_menu: {
                //Calling the my account function
                my_account_function();
                break;
            }

            case R.id.change_password_menu: {
                //Calling the my account function
                change_password_function();
                break;
            }

            case R.id.index_menu: {
                //Calling the my account function
                index_function();
                break;
            }

            case R.id.cart_menu: {
                //Calling the my account function
                cart_function();
                break;
            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    //cart function
    public void cart_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemDetails.this,Cart.class);
        //Redirecting the page
        startActivity(intent);
    }

    //Index function
    public void index_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemDetails.this,MainActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //Change Password function
    public void change_password_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemDetails.this,ChangePasswordActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //My Account Function
    public void my_account_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemDetails.this,MyAccountActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //Home function
    public void home_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemDetails.this,HomeActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    private void setNavigationViewListner() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void logout_function(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Removing all the values in the shared preference
        sharedPreferences.edit().clear().commit();
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemDetails.this,MainActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //----------------------------End of navigation methods----------------------------

    public void setAddToCartButton(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String fullname="";
        String email="";

        if(sharedPreferences.contains("email")){
            add_to_cart_btn.setVisibility(View.VISIBLE);
            no_of_items_selected.setVisibility(View.VISIBLE);
        }else{
            add_to_cart_btn.setVisibility(View.INVISIBLE);
            no_of_items_selected.setVisibility(View.INVISIBLE);
        }
    }

    private void setItem(){
        Items items;
        items = itemController.getItem(item_ID); // set all the values using get method
        item_name.setText(items.getItem_name());
        item_colour.setText("Colour: "+items.getColour());
        item_category.setText("Category: "+items.getItem_category());
        item_size.setText("Size: "+items.getClothing_size());
        item_price.setText("Price: "+items.getPrice());
        Picasso.get()
                .load(items.getPicture_url())
                .resize(500,500)
                .centerCrop()
                .into(item_picture);
    }

    private void addTocart(){
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String email="";
        email = sharedPreferences.getString("email", "");
        int item_id = getIntent().getIntExtra("ITEM_ID", item_ID);
        String amount = no_of_items_selected.getText().toString();
        int quantity = 0;

        if(!amount.equals("")){
            quantity = Integer.parseInt(amount);
        }

        if(quantity == 0){
            cart_model.setItems(itemController.getItem(item_id));
            cart_model.setUser_email(email);
            cart_model.save();
        }
        else if (quantity>0){
            List<Cart_model> carts = new ArrayList<>();
            for(int i = 0 ; i< quantity;i++){
                Cart_model cart_model1 = new Cart_model();
                cart_model1.setItems(itemController.getItem(item_id));
                cart_model1.setUser_email(email);
                carts.add(cart_model1);
            }
            Cart_model.saveInTx(carts);
        }
    }
}
