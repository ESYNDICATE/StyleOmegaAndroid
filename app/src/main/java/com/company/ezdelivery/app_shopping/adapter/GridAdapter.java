package com.company.ezdelivery.app_shopping.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.ezdelivery.app_shopping.R;
import com.company.ezdelivery.app_shopping.controller.ItemController;
import com.company.ezdelivery.app_shopping.model.Items;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GridAdapter extends BaseAdapter {

    List<Items> itemsList;
    private Context context;
    private LayoutInflater inflater;

    public GridAdapter(Context context, List<Items> itemsList) {
        this.context = context;
        this.itemsList = itemsList;
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public Object getItem(int i) {
        return itemsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View gridView = convertView;

        if(convertView == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridView = inflater.inflate(R.layout.items_drawer_custom_layout,null);
        }

        ImageView item_drawer_picture = gridView.findViewById(R.id.items_drawer_picture);
        TextView item_drawer_name = gridView.findViewById(R.id.items_drawer_name);

        String url = itemsList.get(i).getPicture_url();

        Picasso.get().load(url).resize(300,300).into(item_drawer_picture);
        item_drawer_name.setText(itemsList.get(i).getItem_name());

        return gridView;
    }
}
