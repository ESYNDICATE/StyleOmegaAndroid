package com.company.ezdelivery.app_shopping;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.ezdelivery.app_shopping.controller.ItemController;
import com.company.ezdelivery.app_shopping.model.Items;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private TextView userNameView;
    private TextView userEmailView ;
    private ImageButton men_type_btn;
    private ImageButton women_type_btn;
    private ImageButton kids_type_btn;
    private ImageButton accessories_type_btn;
    public static final String MyPREFERENCES = "MyPrefs" ;
    MenuItem nav_logout_menu;
    MenuItem nav_my_account_menu;
    MenuItem nav_change_password_menu;
    private MenuItem cart_menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setNavigationViewListner();

        men_type_btn = findViewById(R.id.men_type_btn);
        women_type_btn = findViewById(R.id.women_type_btn);
        kids_type_btn = findViewById(R.id.kids_type_btn);
        accessories_type_btn = findViewById(R.id.accessories_type_btn);
        ItemController itemController = new ItemController();
        itemController.populateTable();
        setPictures();

        men_type_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, ItemsDrawer.class);
                intent.putExtra("CATEGORY_NAME","Men");
                startActivity(intent);
            }
        });

        women_type_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, ItemsDrawer.class);
                intent.putExtra("CATEGORY_NAME","Women");
                startActivity(intent);
            }
        });

        kids_type_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, ItemsDrawer.class);
                intent.putExtra("CATEGORY_NAME","Kids");
                startActivity(intent);
            }
        });
        accessories_type_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, ItemsDrawer.class);
                intent.putExtra("CATEGORY_NAME","Accessories");
                startActivity(intent);
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawyer);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        userEmailView = (TextView) headerView.findViewById(R.id.userEmailID);
        userNameView = (TextView) headerView.findViewById(R.id.userNameID);

        Menu menuNav=navigationView.getMenu();
        nav_logout_menu = menuNav.findItem(R.id.logout_menu);
        nav_my_account_menu = menuNav.findItem(R.id.my_account_menu);
        nav_change_password_menu = menuNav.findItem(R.id.change_password_menu);
        cart_menu = menuNav.findItem(R.id.cart_menu);


        setSharedPreferenceValues();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Function to set the values retreiving from the shared preference
    public void setSharedPreferenceValues(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String fullname="";
        String email="";

        if(sharedPreferences.contains("email")){
            email = sharedPreferences.getString("email","");
            fullname = sharedPreferences.getString("full_name","");
            //Enabling the view for un registered users
            nav_logout_menu.setVisible(true);
            nav_my_account_menu.setVisible(true);
            nav_change_password_menu.setVisible(true);
            cart_menu.setVisible(true);

        }else{
            fullname = "Guest";
            email = " ";
            //Disabling the view for un registered users
            nav_logout_menu.setVisible(false);
            nav_my_account_menu.setVisible(false);
            nav_change_password_menu.setVisible(false);
            cart_menu.setVisible(false);

        }


        //Setting the values
        userEmailView.setText(email);
        userNameView.setText(fullname);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout_menu: {
                //Calling the logout function
                logout_function();
                break;
            }

            case R.id.home_menu: {
                //Calling the home function
                home_function();
                break;
            }

            case R.id.my_account_menu: {
                //Calling the my account function
                my_account_function();
                break;
            }

            case R.id.change_password_menu: {
                //Calling the my account function
                change_password_function();
                break;
            }

            case R.id.index_menu: {
                //Calling the my account function
                index_function();
                break;
            }

            case R.id.cart_menu: {
                //Calling the my account function
                cart_function();
                break;
            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    //cart function
    public void cart_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(HomeActivity.this,Cart.class);
        //Redirecting the page
        startActivity(intent);
    }

    //Index function
    public void index_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(HomeActivity.this,MainActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //Change Password function
    public void change_password_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(HomeActivity.this,ChangePasswordActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //My Account Function
    public void my_account_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(HomeActivity.this,MyAccountActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //Home function
    public void home_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(HomeActivity.this,HomeActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    private void setNavigationViewListner() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void logout_function(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Removing all the values in the shared preference
        sharedPreferences.edit().clear().commit();
        //Setting the intent to redirect the page
        Intent intent = new Intent(HomeActivity.this,MainActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    public void setPictures(){

        Picasso.get().load(R.drawable.men_shop).resize(350,350).centerCrop().into(men_type_btn);
        Picasso.get().load(R.drawable.women_shopping).resize(350,350).into(women_type_btn);
        Picasso.get().load(R.drawable.kids_shopping).resize(350,350).into(kids_type_btn);
        Picasso.get().load(R.drawable.accessories_shopping).resize(350,350).into(accessories_type_btn);

    }


}
