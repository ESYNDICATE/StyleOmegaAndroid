package com.company.ezdelivery.app_shopping.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.company.ezdelivery.app_shopping.Cart;
import com.company.ezdelivery.app_shopping.R;
import com.company.ezdelivery.app_shopping.controller.CartModelController;
import com.company.ezdelivery.app_shopping.controller.ItemController;
import com.company.ezdelivery.app_shopping.model.Cart_model;
import com.company.ezdelivery.app_shopping.model.Items;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CartAdapter extends BaseAdapter {

    List<Cart_model> itemsList;
    private Context context;
    private LayoutInflater inflater;
    View cartView;
    private int number;

    public CartAdapter(List<Cart_model> itemsList, Context context) {
        this.itemsList = itemsList;
        this.context = context;
    }



    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public Object getItem(int i) {
        return itemsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

         cartView = convertView;

        final int listSize = itemsList.size();
        number =0;

        if(convertView == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            cartView = inflater.inflate(R.layout.cart_custom_layout,null);
        }

        ImageView cart_picture = cartView.findViewById(R.id.cart_image);
        TextView cart_name = cartView.findViewById(R.id.cart_name);
        TextView cart_price = cartView.findViewById(R.id.cart_price);
        ImageButton imageButton = cartView.findViewById(R.id.cart_image_button);



        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemid = Long.toString(itemsList.get(i).getId());
                System.out.println(itemid);
                Cart_model.executeQuery("DELETE FROM cart_title WHERE ID = ?", itemid);
                Intent intent = new Intent(context, Cart.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                view.getContext().startActivity(intent);

            }
        });



        String url = itemsList.get(i).getItems().getPicture_url();

        Picasso.get().load(url).resize(150,150).into(cart_picture);
        cart_name.setText(itemsList.get(i).getItems().getItem_name());
        double item_price = itemsList.get(i).getItems().getPrice();
        String price = "\n"+Double.toString(item_price);
        cart_price.setText(price);

        return cartView;
    }
}
