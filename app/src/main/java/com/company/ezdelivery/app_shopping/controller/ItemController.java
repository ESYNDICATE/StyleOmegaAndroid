package com.company.ezdelivery.app_shopping.controller;

import android.support.annotation.NonNull;

import com.company.ezdelivery.app_shopping.model.Items;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ItemController {

    //Variables
    private Items items_model;
    private int item_id;
    private String item_name;
    private String picture_url;
    private String colour;
    private String item_category;
    private String clothing_size;
    private double price;
    private int item_quantity;

    //Constructor
    public ItemController() {
        this.items_model = new Items();
    }

    //Setters

    public void setItems_model(Items items_model) {
        this.items_model = items_model;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setItem_category(String item_category) {
        this.item_category = item_category;
    }

    public void setClothing_size(String clothing_size) {
        this.clothing_size = clothing_size;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setItem_quantity(int item_quantity) {
        this.item_quantity = item_quantity;
    }

    //Getters

    public Items getItems_model() {
        return items_model;
    }

    public int getItem_id() {
        return item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public String getColour() {
        return colour;
    }

    public String getItem_category() {
        return item_category;
    }

    public String getClothing_size() {
        return clothing_size;
    }

    public double getPrice() {
        return price;
    }

    public int getItem_quantity() {
        return item_quantity;
    }

    //Methods

    public boolean isItemAvailable(String itemID){

        List<Items> itemsList;
        boolean flag = false;
        itemsList = Items.find(Items.class,"item_id = ?",itemID);
        if(itemsList.size() != 0){
            if(itemsList.get(0).getItem_quantity() != 0){
                flag = true;
            }

        }
        return flag;
    }

    public List<Items> getAllItems(){

        List<Items> itemsList = Items.listAll(Items.class);
        return itemsList;
    }

    public Items getItem(int item_ID){
        Items item = new Items();
        String itemID = Integer.toString(item_ID);

        if(Items.find(Items.class,"item_id = ?",itemID).size() != 0){
            item = Items.find(Items.class,"item_id = ?",itemID).get(0);
        }

        return item;
    }

    public void populateTable(){


        List<Items> itemsList = new ArrayList<>();

        if(getAllItems().size() == 1){
            Items.deleteAll(Items.class);
        }

        if (getAllItems().size() == 0){


            //*************** Women's Clothes ***************

            //Item 001
            Items items001 = new Items();
            items001.setItem_id(1);
            items001.setItem_name("Women's T-Shirt");
            items001.setPicture_url("https://filson-a.imgix.net/media/catalog/product/2/0/20049638redclay-main_2.jpg");
            items001.setColour("Pink");
            items001.setItem_category("Women");
            items001.setClothing_size("S");
            items001.setPrice(12.00);
            items001.setItem_quantity(12);
            itemsList.add(items001);


            //Item 002
            Items items002 = new Items();
            items002.setItem_id(2);
            items002.setItem_name("Women's Workout Trouser");
            items002.setPicture_url("https://img.newfrog.com/products/1/1/9/119670.01/119670.01-1.jpg");
            items002.setColour("Grey");
            items002.setItem_category("Women");
            items002.setClothing_size("M");
            items002.setPrice(50.00);
            items002.setItem_quantity(12);
            itemsList.add(items002);

            //Item 003
            Items items003 = new Items();
            items003.setItem_id(3);
            items003.setItem_name("Women's Dress");
            items003.setPicture_url("https://uidesign.gbtcdn.com/GB/images/index/2017/category/170829/Womens_Dresses.jpg");
            items003.setColour("Blue");
            items003.setItem_category("Women");
            items003.setClothing_size("M");
            items003.setPrice(70.00);
            items003.setItem_quantity(12);
            itemsList.add(items003);

            //Item 004
            Items items004 = new Items();
            items004.setItem_id(4);
            items004.setItem_name("Women's Shirt");
            items004.setPicture_url("https://assets.academy.com/mgen/61/20057861.jpg");
            items004.setColour("Pink");
            items004.setItem_category("Women");
            items004.setClothing_size("M");
            items004.setPrice(20.00);
            items004.setItem_quantity(12);
            itemsList.add(items004);

            //Item 005
            Items items005 = new Items();
            items005.setItem_id(5);
            items005.setItem_name("Women's Jeans");
            items005.setPicture_url("https://www.twistedsoul.com/images/womens-blue-high-rise-disco-fit-knee-rip-jeans-p31999-62148_image.jpg");
            items005.setColour("Blue");
            items005.setItem_category("Women");
            items005.setClothing_size("M");
            items005.setPrice(75.00);
            items005.setItem_quantity(12);
            itemsList.add(items005);

            //*************** Men's Clothes ***************

            //Item 006
            Items items006 = new Items();
            items006.setItem_id(6);
            items006.setItem_name("Men's T-Shirt");
            items006.setPicture_url("https://cdn2.bigcommerce.com/n-biq04i/lk0gwzb/products/1645/images/1987/ORANGE__95465.1411339606.380.500.jpg");
            items006.setColour("Orange");
            items006.setItem_category("Men");
            items006.setClothing_size("L");
            items006.setPrice(10.00);
            items006.setItem_quantity(12);
            itemsList.add(items006);

            //Item 007
            Items items007 = new Items();
            items007.setItem_id(7);
            items007.setItem_name("Men's Trouser");
            items007.setPicture_url("https://www.kingsize.com.au/user/images/6962_1000_1000.jpg");
            items007.setColour("Grey");
            items007.setItem_category("Men");
            items007.setClothing_size("L");
            items007.setPrice(35.00);
            items007.setItem_quantity(12);
            itemsList.add(items007);

            //Item 008
            Items items008 = new Items();
            items008.setItem_id(8);
            items008.setItem_name("Men's Shirt");
            items008.setPicture_url("https://ae01.alicdn.com/kf/HTB1Ts5ccfBNTKJjSszeq6Au2VXaX/New-Business-Men-Dress-Long-Sleeve-Shirts-Men-Brand-Clothing-Male-Social-Casual-Shirt-Mens-Clothes.jpg");
            items008.setColour("Blue");
            items008.setItem_category("Men");
            items008.setClothing_size("L");
            items008.setPrice(50.00);
            items008.setItem_quantity(12);
            itemsList.add(items008);

            //Item 009
            Items items009 = new Items();
            items009.setItem_id(9);
            items009.setItem_name("Men's Jumper");
            items009.setPicture_url("https://assets.academy.com/mgen/75/20083275.jpg");
            items009.setColour("Blue");
            items009.setItem_category("Men");
            items009.setClothing_size("L");
            items009.setPrice(80.00);
            items009.setItem_quantity(12);
            itemsList.add(items009);

            //Item 010
            Items items010 = new Items();
            items010.setItem_id(10);
            items010.setItem_name("Men's Shirt");
            items010.setPicture_url("http://www.lightscameraaudition.co.uk/images/uk2/High%20quality%20Jack%20Jones%20Mens%20Clothes%20Shirts%20blue%20Outlet%20385.jpg");
            items010.setColour("Blue");
            items010.setItem_category("Men");
            items010.setClothing_size("L");
            items010.setPrice(70.00);
            items010.setItem_quantity(12);
            itemsList.add(items010);

            //*************** Kid's Clothes ***************

            //Item 011
            Items items011 = new Items();
            items011.setItem_id(11);
            items011.setItem_name("Kid's T-Shirt");
            items011.setPicture_url("http://demo.jamminmedia.co.uk/childrens-fashion-store/wp-content/uploads/sites/7/2016/01/tshirt2.jpg");
            items011.setColour("Yellow");
            items011.setItem_category("Kids");
            items011.setClothing_size("M");
            items011.setPrice(15.00);
            items011.setItem_quantity(12);
            itemsList.add(items011);

            //Item 012
            Items items012 = new Items();
            items012.setItem_id(12);
            items012.setItem_name("Kid's Summer Dress");
            items012.setPicture_url("https://ae01.alicdn.com/kf/HTB1OmI4OVXXXXaraFXXq6xXFXXXT/UNINICE-Summer-Girls-Dress-2017-Design-Kids-Clothes-For-Girls-Fashion-Strapless-Embroidery-Printing-Dress-Princess.jpg");
            items012.setColour("Blue");
            items012.setItem_category("Kids");
            items012.setClothing_size("M");
            items012.setPrice(35.00);
            items012.setItem_quantity(12);
            itemsList.add(items012);

            //Item 013
            Items items013 = new Items();
            items013.setItem_id(13);
            items013.setItem_name("Kid's Harry Potter Hoodie");
            items013.setPicture_url("https://img.customon.com/img/11155005/35271,35,0,0,23,136,114.4752308984,17,32.762384550798,3ececf3c371354e601d8a72fc11895c0/pic/harry-potter-kids-hoodie-gray.jpg");
            items013.setColour("Grey");
            items013.setItem_category("Kids");
            items013.setClothing_size("M");
            items013.setPrice(25.00);
            items013.setItem_quantity(12);
            itemsList.add(items013);

            //Item 014
            Items items014 = new Items();
            items014.setItem_id(14);
            items014.setItem_name("Kid's Shirt and Shorts");
            items014.setPicture_url("https://jamaicaclassifiedonline.com/images/2017/10/01/38887/thumb_kids-clothes-for-sale-2t-4t-2kn9mfls_5.jpg");
            items014.setColour("Blue");
            items014.setItem_category("Kids");
            items014.setClothing_size("M");
            items014.setPrice(35.00);
            items014.setItem_quantity(12);
            itemsList.add(items014);

            //Item 015
            Items items015 = new Items();
            items015.setItem_id(15);
            items015.setItem_name("Kid's Dress");
            items015.setPicture_url("https://www.heladoderretido.com/image/cache/data/products/nadadelazosAW17_plain__DSC0216-1100x1100.jpg");
            items015.setColour("Blue");
            items015.setItem_category("Kids");
            items015.setClothing_size("M");
            items015.setPrice(50.00);
            items015.setItem_quantity(12);
            itemsList.add(items015);

            //*************** Accessories ***************

            //Item 016
            Items items016 = new Items();
            items016.setItem_id(16);
            items016.setItem_name("Owl Drop Earrings");
            items016.setPicture_url("https://ae01.alicdn.com/wsphoto/v0/32269456647_1/New-Fashion-Bohemian-Vintage-Rhinestone-owl-drop-earrings-jewelry-Colorful-Beads-gem-Alloy-long-earring-Wholesale.jpg_350x350.jpg");
            items016.setColour("Red");
            items016.setItem_category("Accessories");
            items016.setClothing_size("M");
            items016.setPrice(20.00);
            items016.setItem_quantity(12);
            itemsList.add(items016);

            //Item 017
            Items items017 = new Items();
            items017.setItem_id(17);
            items017.setItem_name("Necklace");
            items017.setPicture_url("https://gloimg.rglcdn.com/rosegal/pdm-product-pic/Clothing/2017/10/23/goods-img/1508732593000688877.jpg");
            items017.setColour("Blue");
            items017.setItem_category("Accessories");
            items017.setClothing_size("M");
            items017.setPrice(150.00);
            items017.setItem_quantity(12);
            itemsList.add(items017);

            //Item 018
            Items items018 = new Items();
            items018.setItem_id(18);
            items018.setItem_name("MVMT Watch");
            items018.setPicture_url("https://cdn.shopify.com/s/files/1/0377/2037/products/WhiteTanLeather.Front_1024x.jpg?v=1510683461");
            items018.setColour("White");
            items018.setItem_category("Accessories");
            items018.setClothing_size("M");
            items018.setPrice(250.00);
            items018.setItem_quantity(12);
            itemsList.add(items018);

            //Item 019
            Items items019 = new Items();
            items019.setItem_id(19);
            items019.setItem_name("Omega Speedmaster Professional Moonwatch");
            items019.setPicture_url("https://b34959663f5a3997bd0d-2668915a1d3a077262c88fab6aa0aa02.ssl.cf3.rackcdn.com/17331157_1_640.jpg");
            items019.setColour("Black");
            items019.setItem_category("Accessories");
            items019.setClothing_size("M");
            items019.setPrice(5000.00);
            items019.setItem_quantity(12);
            itemsList.add(items019);

            //Item 020
            Items items020 = new Items();
            items020.setItem_id(20);
            items020.setItem_name("Leather Belt");
            items020.setPicture_url("https://cdn.shopify.com/s/files/1/1171/6948/products/a2755brn-1_8b1f1681-bc62-410a-a3a2-c1146b50a3f0.jpg?v=1493391142");
            items020.setColour("Brown");
            items020.setItem_category("Accessories");
            items020.setClothing_size("M");
            items020.setPrice(50.00);
            items020.setItem_quantity(12);
            itemsList.add(items020);

            SugarRecord.saveInTx(itemsList);

        }
    }
}
