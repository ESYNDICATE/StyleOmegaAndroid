package com.company.ezdelivery.app_shopping.model;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

@Table(name = "cart_title")
public class Cart_model extends SugarRecord {

    @Column(name = "items")
    private Items items;
    @Column(name = "user_email")
    private String user_email;

    public Cart_model() {
    }

    public Items getItems() {
        return items;
    }

    public void setItems(Items items) {
        this.items = items;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }
}
