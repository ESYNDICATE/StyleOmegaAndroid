package com.company.ezdelivery.app_shopping;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.company.ezdelivery.app_shopping.adapter.GridAdapter;
import com.company.ezdelivery.app_shopping.controller.ItemController;
import com.company.ezdelivery.app_shopping.model.Items;

import java.util.List;

public class ItemsDrawer extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{

    GridView gridView;
    List<Items> itemsList;
    //----------------------------Navigation Drawer items--------------------------------
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private TextView userNameView;
    private TextView userEmailView ;
    private MenuItem nav_logout_menu;
    private MenuItem nav_my_account_menu;
    private MenuItem nav_change_password_menu;
    private MenuItem cart_menu;
    public static final String MyPREFERENCES = "MyPrefs" ;
    //----------------------------End of navigation items----------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_drawer);




        //----------------------------Navigation components----------------------------
        setNavigationViewListner();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawyer);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        userEmailView = (TextView) headerView.findViewById(R.id.userEmailID);
        userNameView = (TextView) headerView.findViewById(R.id.userNameID);

        Menu menuNav=navigationView.getMenu();
        nav_logout_menu = menuNav.findItem(R.id.logout_menu);
        nav_my_account_menu = menuNav.findItem(R.id.my_account_menu);
        nav_change_password_menu = menuNav.findItem(R.id.change_password_menu);
        cart_menu = menuNav.findItem(R.id.cart_menu);

        setSharedPreferenceValues();

        //----------------------------End of navigation components----------------------------


        getItemsList(getIntent().getStringExtra("CATEGORY_NAME"));

        gridView = findViewById(R.id.items_drawer_grid);
        final GridAdapter adapter = new GridAdapter(ItemsDrawer.this,itemsList);

        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Items item = (Items) adapter.getItem(i);
                int item_Id = item.getItem_id();
                Intent intent = new Intent(ItemsDrawer.this, ItemDetails.class);
                intent.putExtra("ITEM_ID",item_Id);
                startActivity(intent);
            }
        });

    }

    //----------------------------Navigation methods----------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Function to set the values retreiving from the shared preference
    public void setSharedPreferenceValues(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String fullname="";
        String email="";

        if(sharedPreferences.contains("email")){
            email = sharedPreferences.getString("email","");
            fullname = sharedPreferences.getString("full_name","");
            //Enabling the view for un registered users
            nav_logout_menu.setVisible(true);
            nav_my_account_menu.setVisible(true);
            nav_change_password_menu.setVisible(true);
            cart_menu.setVisible(true);

        }else{
            fullname = "Guest";
            email = " ";
            //Disabling the view for un registered users
            nav_logout_menu.setVisible(false);
            nav_my_account_menu.setVisible(false);
            nav_change_password_menu.setVisible(false);
            cart_menu.setVisible(false);

        }


        //Setting the values
        userEmailView.setText(email);
        userNameView.setText(fullname);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout_menu: {
                //Calling the logout function
                logout_function();
                break;
            }

            case R.id.home_menu: {
                //Calling the home function
                home_function();
                break;
            }

            case R.id.my_account_menu: {
                //Calling the my account function
                my_account_function();
                break;
            }

            case R.id.change_password_menu: {
                //Calling the my account function
                change_password_function();
                break;
            }

            case R.id.index_menu: {
                //Calling the my account function
                index_function();
                break;
            }

            case R.id.cart_menu: {
                //Calling the my account function
                cart_function();
                break;
            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
    //cart function
    public void cart_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemsDrawer.this,Cart.class);
        //Redirecting the page
        startActivity(intent);
    }

    //Index function
    public void index_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemsDrawer.this,MainActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //Change Password function
    public void change_password_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemsDrawer.this,ChangePasswordActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //My Account Function
    public void my_account_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemsDrawer.this,MyAccountActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //Home function
    public void home_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemsDrawer.this,HomeActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    private void setNavigationViewListner() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void logout_function(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Removing all the values in the shared preference
        sharedPreferences.edit().clear().commit();
        //Setting the intent to redirect the page
        Intent intent = new Intent(ItemsDrawer.this,MainActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //----------------------------End of navigation methods----------------------------


    public void getItemsList(String category){

        itemsList = Items.find(Items.class,"item_category = ?",category);
    }
}
