package com.company.ezdelivery.app_shopping;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.company.ezdelivery.app_shopping.model.Cart_model;
import com.company.ezdelivery.app_shopping.model.ShippingInfo;

public class Checkout extends AppCompatActivity {

    private EditText txt_address;
    private EditText txt_city;
    private EditText txt_state;
    private EditText txt_zipcode;
    private EditText txt_country;
    private EditText txt_cvv;
    private EditText txt_CreditCard;
    private ShippingInfo shippingInfo;
    private Button pay_button;

    private String email;
    public static final String MyPREFERENCES = "MyPrefs" ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        shippingInfo = new ShippingInfo();
        txt_address = findViewById(R.id.txt_address1);
        txt_city = findViewById(R.id.txt_City);
        txt_state = findViewById(R.id.txt_State);
        txt_country = findViewById(R.id.txt_Country);
        txt_zipcode = findViewById(R.id.txt_Zipcode);
        txt_cvv = findViewById(R.id.txt_CVV);
        txt_CreditCard = findViewById(R.id.txt_CreditCardNumber);
        pay_button = findViewById(R.id.pay_btn);

        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        email = sharedPreferences.getString("email", "");

        pay_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Check if the cvv has 3 digits
                int cvv_length = txt_cvv.length();
                int card_length = txt_CreditCard.length();

                if(cvv_length == 3){
                    Toast.makeText(Checkout.this,"CVV must contain 3 digits",Toast.LENGTH_LONG).show();
                }else{
                    if(card_length == 16){
                        Toast.makeText(Checkout.this,"Card Number must contain 16 digits",Toast.LENGTH_LONG).show();
                    }else{
                        Cart_model.executeQuery("DELETE FROM cart_title WHERE user_email = ?", email);
                        setLocation();
                        Intent intent = new Intent(Checkout.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                }


            }
        });



    }



    private void setLocation(){

        shippingInfo.setAddress(txt_address.getText().toString());
        shippingInfo.setCity(txt_city.getText().toString());
        shippingInfo.setCountry(txt_country.getText().toString());
        shippingInfo.setState(txt_state.getText().toString());

        if(txt_zipcode.getText().toString().equals("")){
            shippingInfo.setZipcode(0);
        }
        else{
            shippingInfo.setZipcode(Integer.parseInt(txt_zipcode.getText().toString()));
        }


        shippingInfo.save();
    }
}
