package com.company.ezdelivery.app_shopping;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.company.ezdelivery.app_shopping.controller.UserController;

public class ChangePasswordActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //Declaring components
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    EditText current_password, new_password,confirm_password;
    Button btn_change , btn_back;
    private View mProgressView;
    private View chnageFormView;
    private TextView userNameView;
    private TextView userEmailView ;
    MenuItem nav_logout_menu;
    MenuItem nav_my_account_menu;
    MenuItem nav_change_password_menu;
    public static final String MyPREFERENCES = "MyPrefs" ;
    private MenuItem cart_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        //------------------Navigation methods -------------------------
        setNavigationViewListner();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawyer);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        userEmailView = (TextView) headerView.findViewById(R.id.userEmailID);
        userNameView = (TextView) headerView.findViewById(R.id.userNameID);

        Menu menuNav=navigationView.getMenu();
        nav_logout_menu = menuNav.findItem(R.id.logout_menu);
        nav_my_account_menu = menuNav.findItem(R.id.my_account_menu);
        nav_change_password_menu = menuNav.findItem(R.id.change_password_menu);
        cart_menu = menuNav.findItem(R.id.cart_menu);

        setSharedPreferenceValues();

        //------------------End of Navigation methods -------------------------


        //Getting the components based on the ID
        current_password = findViewById(R.id.user_current_password);
        new_password = findViewById(R.id.user_new_password);
        confirm_password = findViewById(R.id.user_confirm_new_password);
        btn_change = findViewById(R.id.change_button);

        chnageFormView = findViewById(R.id.change_password_form);
        mProgressView = findViewById(R.id.change_password_progress);

        changeSubmitAction();
    }

    public void changeSubmitAction(){
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Getting the user input details for local variables
                String cPassword = current_password.getText().toString().trim();
                String nPassword = new_password.getText().toString().trim();
                String confirmPassword = confirm_password.getText().toString().trim();

                // Reset errors.
                current_password.setError(null);
                new_password.setError(null);
                confirm_password.setError(null);

                boolean cancel = false;
                View focusView = null;


                //Creating the Shared Preference object
                SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                //Getting the email
                String email = sharedPreferences.getString("email","");

                //Creating an object of the user controller
                UserController userController = new UserController();
                //Setting the values to the created user controller

                userController.setEmail(email);
                userController.setCurrentPasswordBasedOnEmail();


                // Check if the current password is empty.
                if (TextUtils.isEmpty(cPassword)) {
                    current_password.setError(getString(R.string.error_field_required));
                    focusView = current_password;
                    cancel = true;
                } else if (!userController.getPassword().toString().equals(cPassword)){
                    current_password.setError("Password is incorrect");
                    focusView = current_password;
                    cancel = true;
                }

                // Check if the new password is empty.
                if (TextUtils.isEmpty(nPassword)) {
                    confirm_password.setError(getString(R.string.error_field_required));
                    focusView = confirm_password;
                    cancel = true;
                }

                // Check if the confirm password is empty.
                if (TextUtils.isEmpty(confirmPassword)) {
                    new_password.setError(getString(R.string.error_field_required));
                    focusView = new_password;
                    cancel = true;
                } else if (!confirmPassword.equals(nPassword)){
                    new_password.setError("Password does not match");
                    focusView = new_password;
                    cancel = true;
                }




                if (cancel) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                } else {
                    // Show a progress spinner, and kick off a background task to
                    // perform the user login attempt.
                    showProgress(true);



                    //Calling the update method in the user controller object
                    userController.updateUserPasswordDetails(nPassword);


                    String fullname = userController.getFullNameBasedOnEmail();

                    //Setting the values to shared preference
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("full_name", fullname);
                    editor.commit();

                    //Display the successful message
                    Toast.makeText(ChangePasswordActivity.this, "Successfully Updated", Toast.LENGTH_SHORT).show();
                    //Setting the redirect page to the Login Activity Page
                    Intent intent = new Intent(ChangePasswordActivity.this, HomeActivity.class);
                    //Executing the method of redirecting
                    startActivity(intent);
                    finish();

                }
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            chnageFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            chnageFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    chnageFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            chnageFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    //------------------Navigation methods bodies -------------------------
    private void setNavigationViewListner() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Function to set the values retreiving from the shared preference
    public void setSharedPreferenceValues(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String fullname="";
        String email="";

        if(sharedPreferences.contains("email")){
            email = sharedPreferences.getString("email","");
            fullname = sharedPreferences.getString("full_name","");
            //Enabling the view for un registered users
            nav_logout_menu.setVisible(true);
            nav_my_account_menu.setVisible(true);
            nav_change_password_menu.setVisible(true);
            cart_menu.setVisible(true);

        }else{
            fullname = "Guest";
            email = " ";
            //Disabling the view for un registered users
            nav_logout_menu.setVisible(false);
            nav_my_account_menu.setVisible(false);
            nav_change_password_menu.setVisible(false);
            cart_menu.setVisible(false);

        }


        //Setting the values
        userEmailView.setText(email);
        userNameView.setText(fullname);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout_menu: {
                //Calling the logout function
                logout_function();
                break;
            }

            case R.id.home_menu: {
                //Calling the home function
                home_function();
                break;
            }

            case R.id.my_account_menu: {
                //Calling the my account function
                my_account_function();
                break;
            }

            case R.id.change_password_menu: {
                //Calling the my account function
                change_password_function();
                break;
            }

            case R.id.index_menu: {
                //Calling the my account function
                index_function();
                break;
            }

            case R.id.cart_menu: {
                //Calling the my account function
                cart_function();
                break;
            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    //cart function
    public void cart_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChangePasswordActivity.this,Cart.class);
        //Redirecting the page
        startActivity(intent);
    }


    //Index function
    public void index_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChangePasswordActivity.this,MainActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //Change Password function
    public void change_password_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChangePasswordActivity.this,ChangePasswordActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //My Account Function
    public void my_account_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChangePasswordActivity.this,MyAccountActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //Home function
    public void home_function(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChangePasswordActivity.this,HomeActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    public void logout_function(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Removing all the values in the shared preference
        sharedPreferences.edit().clear().commit();
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChangePasswordActivity.this,MainActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //------------------End of Navigation methods -------------------------
}
